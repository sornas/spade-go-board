module top (
    input i_clk,

    input i_Switch_1,
    input i_Switch_2,
    input i_Switch_3,
    input i_Switch_4,
    output o_LED_1,
    output o_LED_2,
    output o_LED_3,
    output o_LED_4
);
    reg rst = 1;
    always @(posedge i_clk) begin
        rst <= 0;
    end

    e_main main
        ( ._i_clk(i_clk)
        , ._i_rst(rst)
        , ._i_in_unsync(i_Switch_1)
        , .__output({ o_LED_1, o_LED_2, o_LED_3, o_LED_4 })
        // , .__output(o_LED_1)
        );
endmodule
